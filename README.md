# Finaro - Payment Gateway

## About
Service provides API for the submitting the payments and retrieving already submitted payments.

## API
Entry point: PaymentController.class

## Tests
Service partly covered by test.

PaymentControllerTest - checking validation functionality.

PaymentGatewayIT - checking full flow.

Test data:
- src/test/resources/com/finaro/paymentgateway/controller
- src/test/resources/com/finaro/paymentgateway/it

## AUDIT
There is a functionality which log some already submitted payments to 'audit.log' file. 
Path of the file is configurable inside application.yaml

audit:
    logging:
        file-path: "audit.log"

## PCI 
Sensitive data was hidden or partly mask before logging or responding to the client. 
In order to check it, you need: 
1. Run 'PaymentGatewayIT.whenSubmitPayment_thenReturns200'. 
2. Check the 'audit.log' file.


## Requirements
The detailed requirements are found in [ASSIGNMENT.md](ASSIGNMENT.md).

