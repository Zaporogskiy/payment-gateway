package com.finaro.paymantgateway.repository;

import com.finaro.paymantgateway.model.PaymentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionRepository extends CrudRepository<PaymentEntity, Long> {

    Optional<PaymentEntity> findByInvoice(Long invoiceId);
}