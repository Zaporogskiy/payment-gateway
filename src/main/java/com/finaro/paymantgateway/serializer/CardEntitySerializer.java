package com.finaro.paymantgateway.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.finaro.paymantgateway.model.CardEntity;

import java.io.IOException;

public class CardEntitySerializer extends StdSerializer<CardEntity> {

    public CardEntitySerializer() {
        super(CardEntity.class);
    }

    @Override
    public void serialize(CardEntity value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String pan = value.getPan();
        int cuttingIndex = pan.length() - 4;
        String hiddenPan = "*".repeat(pan.substring(0, cuttingIndex).length()) + pan.substring(cuttingIndex);
        String hiddenExpiry = "*".repeat(value.getExpiry().length());

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("pan", hiddenPan);
        jsonGenerator.writeStringField("expiry", hiddenExpiry);
        jsonGenerator.writeEndObject();
    }
}
