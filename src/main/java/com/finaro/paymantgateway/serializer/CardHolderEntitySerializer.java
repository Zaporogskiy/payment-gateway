package com.finaro.paymantgateway.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.finaro.paymantgateway.model.CardHolderEntity;

import java.io.IOException;

public class CardHolderEntitySerializer extends StdSerializer<CardHolderEntity> {

    public CardHolderEntitySerializer() {
        super(CardHolderEntity.class);
    }

    @Override
    public void serialize(CardHolderEntity value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String name = value.getName();
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", "*".repeat(name.length()));
        jsonGenerator.writeStringField("email", value.getEmail());
        jsonGenerator.writeEndObject();
    }
}