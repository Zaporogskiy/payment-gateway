package com.finaro.paymantgateway.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.finaro.paymantgateway.dto.CardDto;

import java.io.IOException;

public class CardDtoSerializer extends StdSerializer<CardDto> {

    public CardDtoSerializer() {
        super(CardDto.class);
    }

    @Override
    public void serialize(CardDto value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String pan = value.getPan();
        int cuttingIndex = pan.length() - 4;
        String hiddenPan = "*".repeat(pan.substring(0, cuttingIndex).length()) + pan.substring(cuttingIndex);
        String hiddenExpiry = "*".repeat(value.getExpiry().length());

        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("pan", hiddenPan);
        jsonGenerator.writeStringField("expiry", hiddenExpiry);
        jsonGenerator.writeEndObject();
    }
}