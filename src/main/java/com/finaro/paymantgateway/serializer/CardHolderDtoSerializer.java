package com.finaro.paymantgateway.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.finaro.paymantgateway.dto.CardHolderDto;

import java.io.IOException;

public class CardHolderDtoSerializer extends StdSerializer<CardHolderDto> {

    public CardHolderDtoSerializer() {
        super(CardHolderDto.class);
    }

    @Override
    public void serialize(CardHolderDto value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", "*".repeat(value.getName().length()));
        jsonGenerator.writeStringField("email", value.getEmail());
        jsonGenerator.writeEndObject();
    }
}