package com.finaro.paymantgateway.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.finaro.paymantgateway.model.PaymentEntity;
import com.finaro.paymantgateway.service.AuditService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

@Service
@Slf4j
public class LocalFileAudit implements AuditService {

    @Value("${audit.logging.file-path}")
    private String logFilePath;

    @Override
    public void audit(PaymentEntity savedEntity) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            File file = Paths.get(logFilePath).toFile();
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
            out.println();
            mapper.writeValue(out, savedEntity);
        } catch (IOException e) {
            log.error("Audit was failed. PaymentEntity: {}", savedEntity);
        }
    }
}