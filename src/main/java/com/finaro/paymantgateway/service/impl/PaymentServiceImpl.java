package com.finaro.paymantgateway.service.impl;

import com.finaro.paymantgateway.converter.PaymentConverter;
import com.finaro.paymantgateway.dal.PaymentDal;
import com.finaro.paymantgateway.dto.PaymentDto;
import com.finaro.paymantgateway.dto.SubmitPaymentRequestDto;
import com.finaro.paymantgateway.dto.SubmitPaymentResponseDto;
import com.finaro.paymantgateway.model.PaymentEntity;
import com.finaro.paymantgateway.service.AuditService;
import com.finaro.paymantgateway.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

    private final PaymentDal paymentTransactionDal;
    private final PaymentConverter transactionEntityConverter;
    private final AuditService auditService;

    public PaymentServiceImpl(PaymentDal paymentTransactionDal,
                              PaymentConverter transactionEntityConverter,
                              AuditService auditService) {
        this.paymentTransactionDal = paymentTransactionDal;
        this.transactionEntityConverter = transactionEntityConverter;
        this.auditService = auditService;
    }

    public SubmitPaymentResponseDto submit(SubmitPaymentRequestDto paymentRequestDto) {
        PaymentEntity entity = transactionEntityConverter.convert(paymentRequestDto);
        PaymentEntity savedEntity = paymentTransactionDal.save(entity);

        auditService.audit(savedEntity);
        return new SubmitPaymentResponseDto(true);
    }

    public PaymentDto findPayment(Long invoiceId) {
        Optional<PaymentEntity> entity = paymentTransactionDal.findByInvoiceId(invoiceId);

        return entity.map(transactionEntityConverter::convert).orElse(null);
    }
}
