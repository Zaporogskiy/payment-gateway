package com.finaro.paymantgateway.service;

import com.finaro.paymantgateway.model.PaymentEntity;

public interface AuditService {

    void audit(PaymentEntity savedEntity);
}