package com.finaro.paymantgateway.service;

import com.finaro.paymantgateway.dto.PaymentDto;
import com.finaro.paymantgateway.dto.SubmitPaymentRequestDto;
import com.finaro.paymantgateway.dto.SubmitPaymentResponseDto;

public interface PaymentService {

    SubmitPaymentResponseDto submit(SubmitPaymentRequestDto paymentRequestDto);

    PaymentDto findPayment(Long invoiceId);
}
