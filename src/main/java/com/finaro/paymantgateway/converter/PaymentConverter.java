package com.finaro.paymantgateway.converter;

import com.finaro.paymantgateway.dto.CardDto;
import com.finaro.paymantgateway.dto.CardHolderDto;
import com.finaro.paymantgateway.dto.PaymentDto;
import com.finaro.paymantgateway.dto.SubmitPaymentRequestDto;
import com.finaro.paymantgateway.model.CardEntity;
import com.finaro.paymantgateway.model.CardHolderEntity;
import com.finaro.paymantgateway.model.PaymentEntity;
import org.springframework.stereotype.Component;

@Component
public class PaymentConverter {

    public PaymentEntity convert(SubmitPaymentRequestDto request) {
        PaymentEntity entity = new PaymentEntity();
        CardHolderEntity cardHolderEntity = new CardHolderEntity();
        CardEntity cardEntity = new CardEntity();

        cardHolderEntity.setName(request.getCardholder().getName());
        cardHolderEntity.setEmail(request.getCardholder().getEmail());

        cardEntity.setPan(request.getCard().getPan());
        cardEntity.setExpiry(request.getCard().getExpiry());

        entity.setInvoice(request.getInvoice());
        entity.setAmount(request.getAmount());
        entity.setCurrency(request.getCurrency());
        entity.setCardholder(cardHolderEntity);
        entity.setCard(cardEntity);
        return entity;
    }

    public PaymentDto convert(PaymentEntity entity) {
        String cardHolderName = entity.getCardholder().getName();
        String cardHolderEmail = entity.getCardholder().getEmail();
        String cardPan = entity.getCard().getPan();
        String cardExpiry = entity.getCard().getExpiry();
        CardHolderDto cardHolderDto = new CardHolderDto(cardHolderName, cardHolderEmail);
        CardDto cardDto = new CardDto(cardPan, cardExpiry);

        return new PaymentDto(
                entity.getInvoice(),
                entity.getAmount(),
                entity.getCurrency(),
                cardHolderDto,
                cardDto);
    }
}