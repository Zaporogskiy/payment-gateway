package com.finaro.paymantgateway.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class SubmitPaymentRequestDto {

    @NotNull(message = "Invoice is required.")
    private Long invoice;

    @NotNull(message = "Amount is required.")
    @Min(value = 1L, message = "Amount should be a positive")
    private final Long amount;

    @NotNull(message = "Currency is required.")
    private final String currency;

    @Valid
    @NotNull(message = "Cardholder is required.")
    private final CardHolderDto cardholder;

    @Valid
    @NotNull(message = "Card is required.")
    private final CardDto card;
}