package com.finaro.paymantgateway.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentDto {

    private Long invoice;
    private Long amount;
    private String currency;
    private CardHolderDto cardholder;
    private CardDto card;
}