package com.finaro.paymantgateway.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.finaro.paymantgateway.serializer.CardHolderDtoSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@JsonSerialize(using = CardHolderDtoSerializer.class)
@NoArgsConstructor
@AllArgsConstructor
public class CardHolderDto {

    @NotNull(message = "Name is required.")
    private String name;

    @NotNull(message = "Email is required.")
    @Email(message = "Email should have a valid format")
    private String email;
}