package com.finaro.paymantgateway.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
public class SubmitPaymentResponseDto {

    public SubmitPaymentResponseDto(boolean approved) {
        this.approved = approved;
    }

    public SubmitPaymentResponseDto(boolean approved, Map<String, String> errors) {
        this.approved = approved;
        this.errors = errors;
    }

    private boolean approved;
    private Map<String, String> errors;
}