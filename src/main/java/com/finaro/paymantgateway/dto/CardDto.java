package com.finaro.paymantgateway.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.finaro.paymantgateway.serializer.CardDtoSerializer;
import com.finaro.paymantgateway.validator.CreditCardExpiredDate;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.LuhnCheck;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@JsonSerialize(using = CardDtoSerializer.class)
@NoArgsConstructor
public class CardDto {

    public CardDto(String pan, String expiry) {
        this.pan = pan;
        this.expiry = expiry;
    }

    @NotNull(message = "PAN is required.")
    @Size(min = 16, max = 16, message = "PAN should be 16 digits long.")
    @LuhnCheck(message = "PAN was failed by Luhn check")
    private String pan;

    @NotNull(message = "Expiry date is required.")
    @Size(min = 4, max = 4, message = "Invalid expire date format")
    @CreditCardExpiredDate
    private String expiry;

    @NotNull(message = "CVV is required.")
    private String cvv;
}
