package com.finaro.paymantgateway.dal;

import com.finaro.paymantgateway.model.PaymentEntity;
import com.finaro.paymantgateway.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Slf4j
@Repository
@RequiredArgsConstructor
public class H2PaymentDal implements PaymentDal {

    @Autowired
    private final TransactionRepository transactionRepository;

    @Override
    public PaymentEntity save(PaymentEntity entity) {
        return transactionRepository.save(entity);
    }

    @Override
    public Optional<PaymentEntity> findByInvoiceId(Long invoiceId) {
        return transactionRepository.findByInvoice(invoiceId);
    }
}