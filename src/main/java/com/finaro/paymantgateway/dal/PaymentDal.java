package com.finaro.paymantgateway.dal;

import com.finaro.paymantgateway.model.PaymentEntity;

import java.util.Optional;

public interface PaymentDal {

    PaymentEntity save(PaymentEntity entity);

    Optional<PaymentEntity> findByInvoiceId(Long invoiceId);
}
