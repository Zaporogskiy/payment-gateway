package com.finaro.paymantgateway.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = CreditCardExpiredDateValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CreditCardExpiredDate {

    String message() default "Credit card was expired";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}