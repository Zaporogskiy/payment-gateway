package com.finaro.paymantgateway.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.Date;

public class CreditCardExpiredDateValidator implements ConstraintValidator<CreditCardExpiredDate, String> {

    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {
        try {
            String month = date.substring(0, 2);
            String year = date.substring(2);
            YearMonth yearMonthObject = YearMonth.of(Integer.parseInt(year), Integer.parseInt(month));
            int days = yearMonthObject.lengthOfMonth();

            String input = days + "/" + month + "/" + year;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yy");

            Date creditCardExpiredDate = simpleDateFormat.parse(input);
            return creditCardExpiredDate.after(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}