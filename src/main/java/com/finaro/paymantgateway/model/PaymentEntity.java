package com.finaro.paymantgateway.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Data
@NoArgsConstructor
@Entity(name = "payment")
public class PaymentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Column(name = "invoice")
    private Long invoice;

    @Column(name = "amount")
    private Long amount;

    @Column(name = "currency")
    private String currency;

    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "card_holder_id", referencedColumnName = "id")
    private CardHolderEntity cardholder;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "card_id", referencedColumnName = "id")
    private CardEntity card;

    @Override
    public String toString() {
        return "TransactionEntity{" +
                "id=" + id +
                ", invoice=" + invoice +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", cardholder=" + cardholder +
                ", card=" + card +
                '}';
    }
}
