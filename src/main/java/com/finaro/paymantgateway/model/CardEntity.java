package com.finaro.paymantgateway.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.finaro.paymantgateway.converter.AttributeEncryptor;
import com.finaro.paymantgateway.serializer.CardEntitySerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "card")
@JsonSerialize(using = CardEntitySerializer.class)
public class CardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Convert(converter = AttributeEncryptor.class)
    @Column(name = "pan")
    private String pan;

    @Convert(converter = AttributeEncryptor.class)
    @Column(name = "expiry")
    private String expiry;

    @OneToOne(mappedBy = "card")
    private PaymentEntity transactionEntity;

    @Override
    public String toString() {
        return "CardEntity{" +
                "id=" + id +
                ", pan='" + pan + '\'' +
                ", expiry='" + expiry + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardEntity)) return false;
        CardEntity that = (CardEntity) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getPan(), that.getPan()) && Objects.equals(getExpiry(), that.getExpiry());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPan(), getExpiry());
    }
}