package com.finaro.paymantgateway.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.finaro.paymantgateway.converter.AttributeEncryptor;
import com.finaro.paymantgateway.serializer.CardHolderEntitySerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "cardholder")
@JsonSerialize(using = CardHolderEntitySerializer.class)
public class CardHolderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    @Convert(converter = AttributeEncryptor.class)
    private String name;

    @Column(name = "email")
    private String email;

    @OneToOne(mappedBy = "cardholder")
    private PaymentEntity transactionEntity;

    @Override
    public String toString() {
        return "CardHolderEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardHolderEntity)) return false;
        CardHolderEntity that = (CardHolderEntity) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getEmail(), that.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getEmail());
    }
}

