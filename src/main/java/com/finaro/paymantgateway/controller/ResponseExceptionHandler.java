package com.finaro.paymantgateway.controller;

import com.finaro.paymantgateway.dto.SubmitPaymentResponseDto;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toMap;

@ControllerAdvice
public class ResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleBindException(ex, headers, status, request);
    }

    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        // todo simplify this code
        Map<String, Set<String>> fieldErrors = ex.getFieldErrors().stream()
                .collect(groupingBy(FieldError::getField, mapping(this::getErrorMessage, Collectors.toSet())));

        Map<String, String> errors = fieldErrors.entrySet().stream().collect(toMap(Map.Entry::getKey, e -> String.join(". ", e.getValue())));
        SubmitPaymentResponseDto result = new SubmitPaymentResponseDto(false, errors);

        return ResponseEntity.badRequest().body(result);
    }

    private String getErrorMessage(FieldError err) {
        Optional<String> message;
        if (err.contains(TypeMismatchException.class)) {
            message = getFirstCause(err.unwrap(TypeMismatchException.class), IllegalArgumentException.class)
                    .map(Throwable::getMessage);
        } else {
            message = Optional.ofNullable(err.getDefaultMessage());
        }
        return message.orElse("");
    }

    @SuppressWarnings("unchecked")
    private <T extends Throwable> Optional<T> getFirstCause(Exception ex, Class<T> clazz) {
        List<Throwable> chain = ExceptionUtils.getThrowableList(ex);
        return chain.stream()
                .filter(throwable -> clazz.isAssignableFrom(throwable.getClass()))
                .map(throwable -> (T) throwable)
                .findFirst();
    }
}
