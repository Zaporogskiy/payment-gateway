package com.finaro.paymantgateway.controller;

import com.finaro.paymantgateway.dto.PaymentDto;
import com.finaro.paymantgateway.dto.SubmitPaymentRequestDto;
import com.finaro.paymantgateway.dto.SubmitPaymentResponseDto;
import com.finaro.paymantgateway.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping(path = "/payment", consumes = "application/json")
    public ResponseEntity<SubmitPaymentResponseDto> submitPayment(@Valid @RequestBody SubmitPaymentRequestDto request) {
        SubmitPaymentResponseDto response = paymentService.submit(request);

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/payment")
    public ResponseEntity<PaymentDto> retrieveTransaction(@RequestParam Long invoiceId) {
        PaymentDto payment = paymentService.findPayment(invoiceId);

        return Optional.ofNullable(payment)
                .map(response -> ResponseEntity.ok().body(response))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
