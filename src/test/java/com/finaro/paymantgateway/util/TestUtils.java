package com.finaro.paymantgateway.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.nio.file.Path;

import static java.nio.file.Files.readString;
import static java.nio.file.Paths.get;

public class TestUtils {

    private static final ObjectMapper MAPPER_INSTANCE = new ObjectMapper();

    static {
        MAPPER_INSTANCE.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        MAPPER_INSTANCE.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @SneakyThrows
    public static String readJson(String fileName) {
        Path path = get(TestUtils.class.getClassLoader().getResource(fileName).toURI());
        return readString(path);
    }

    @SneakyThrows
    public static <T> T readJsonToObject(String fileName, TypeReference<T> valueTypeRef) {
        return mapToObject(readJson(fileName), valueTypeRef);
    }

    @SneakyThrows
    public static <T> T mapToObject(String json, TypeReference<T> valueTypeRef) {
        return MAPPER_INSTANCE.readValue(json, valueTypeRef);
    }
}

