package com.finaro.paymantgateway.it;

import com.fasterxml.jackson.core.type.TypeReference;
import com.finaro.paymantgateway.PaymentGatewayApplication;
import com.finaro.paymantgateway.dto.PaymentDto;
import com.finaro.paymantgateway.dto.SubmitPaymentResponseDto;
import com.finaro.paymantgateway.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = PaymentGatewayApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentGatewayIT {

    @LocalServerPort
    private int serverPort;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void whenSubmitPayment_thenReturns200() {
        // given
        String paymentUri = "http://localhost:" + serverPort + "/payment";
        String retrievePaymentUri = "http://localhost:" + serverPort + "/payment?invoiceId={invoiceId}";
        String submitPaymentRequestBody = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_valid_submit_payment_request_dto.json");
        HttpEntity<String> submitPaymentRequest = buildSubmitPaymentRequest(submitPaymentRequestBody);
        SubmitPaymentResponseDto expectedPaymentSubmitResponse = TestUtils.readJsonToObject("com/finaro/paymentgateway/it/expected/expected_valid_submit_payment_response_dto.json", new TypeReference<>() {});
        PaymentDto expectedRetrievedPaymentResponse = TestUtils.readJsonToObject("com/finaro/paymentgateway/it/expected/expected_valid_retrieve_payment_response_dto.json", new TypeReference<>() {});

        // when
        ResponseEntity<SubmitPaymentResponseDto> actualSubmitPaymentResponse = restTemplate.postForEntity(
                paymentUri, submitPaymentRequest, SubmitPaymentResponseDto.class);
        ResponseEntity<PaymentDto> actualRetrievePaymentResponse = restTemplate.getForEntity(
                retrievePaymentUri, PaymentDto.class, Map.of("invoiceId", 2L));

        // then
        assertEquals(HttpStatus.OK, actualSubmitPaymentResponse.getStatusCode());
        assertEquals(expectedPaymentSubmitResponse, actualSubmitPaymentResponse.getBody());

        assertEquals(HttpStatus.OK, actualSubmitPaymentResponse.getStatusCode());
        assertEquals(expectedRetrievedPaymentResponse, actualRetrievePaymentResponse.getBody());
    }

    private HttpEntity<String> buildSubmitPaymentRequest(String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body, headers);
        return request;
    }
}