package com.finaro.paymantgateway.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.finaro.paymantgateway.dto.PaymentDto;
import com.finaro.paymantgateway.dto.SubmitPaymentRequestDto;
import com.finaro.paymantgateway.dto.SubmitPaymentResponseDto;
import com.finaro.paymantgateway.service.PaymentService;
import com.finaro.paymantgateway.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PaymentController.class)
public class PaymentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaymentService paymentService;

    @Test
    void whenValidRequestSubmitPayment_thenReturns200() throws Exception {
        String request = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_valid_submit_payment_request_dto.json");
        SubmitPaymentResponseDto expected = TestUtils.readJsonToObject("com/finaro/paymentgateway/controller/expected/expected_valid_submit_payment_response_dto.json", new TypeReference<>() {});

        when(paymentService.submit(any(SubmitPaymentRequestDto.class))).thenReturn(expected);
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.approved").value(true));
    }

    @Test
    void whenInValidMissedFieldsRequestSubmitPayment_thenReturns400() throws Exception {
        String request = "{}";
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.approved").value(false))
                .andExpect(jsonPath("$.errors.amount").value("Amount is required."))
                .andExpect(jsonPath("$.errors.currency").value("Currency is required."))
                .andExpect(jsonPath("$.errors.invoice").value("Invoice is required."))
                .andExpect(jsonPath("$.errors.cardholder").value("Cardholder is required."))
                .andExpect(jsonPath("$.errors.card").value("Card is required."));
    }

    @Test
    void whenInValidAmountRequestSubmitPayment_thenReturns400() throws Exception {
        String request = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_invalid_amount_submit_payment_request_dto.json");
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.approved").value(false))
                .andExpect(jsonPath("$.errors.amount").value("Amount should be a positive"));
    }

    @Test
    void whenInValidEmailFormatRequestSubmitPayment_thenReturns400() throws Exception {
        String request = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_invalid_email_format_submit_payment_request_dto.json");
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.approved").value(false))
                .andExpect(jsonPath("$['errors'].['cardholder.email']").value("Email should have a valid format"));
    }

    @Test
    void whenInValidPanFormatRequestSubmitPayment_thenReturns400() throws Exception {
        String request = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_invalid_pan_format_submit_payment_request_dto.json");
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.approved").value(false))
                .andExpect(jsonPath("$['errors'].['card.pan']").value("PAN was failed by Luhn check"));
    }

    @Test
    void whenInValidPanSizeRequestSubmitPayment_thenReturns400() throws Exception {
        String request = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_invalid_pan_size_submit_payment_request_dto.json");
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.approved").value(false))
                .andExpect(jsonPath("$['errors'].['card.pan']").value("PAN was failed by Luhn check. PAN should be 16 digits long."));
    }

    @Test
    void whenInValidExpireRequestSubmitPayment_thenReturns400() throws Exception {
        String request = TestUtils.readJson("com/finaro/paymentgateway/controller/given/given_invalid_expire_submit_payment_request_dto.json");
        mockMvc.perform(post("/payment")
                        .contentType("application/json")
                        .content(request))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.approved").value(false))
                .andExpect(jsonPath("$['errors'].['card.expiry']").value("Credit card was expired. Invalid expire date format"));
    }

    @Test
    void whenExistingRetrieveTransaction_thenReturns200() throws Exception {
        Long invoiceId = 1L;
        PaymentDto expected = TestUtils.readJsonToObject("com/finaro/paymentgateway/controller/expected/expected_valid_retrieve_transaction_response_dto.json", new TypeReference<>() {});

        when(paymentService.findPayment(invoiceId)).thenReturn(expected);
        mockMvc.perform(get("/payment")
                        .contentType("application/json")
                        .param("invoiceId", String.valueOf(invoiceId)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.invoice").value(1))
                .andExpect(jsonPath("$.amount").value(1))
                .andExpect(jsonPath("$.currency").value("EUR"))
                .andExpect(jsonPath("$.cardholder.name").value("**********"))
                .andExpect(jsonPath("$.cardholder.email").value("email@domain.com"))
                .andExpect(jsonPath("$.card.pan").value("************3766"))
                .andExpect(jsonPath("$.card.expiry").value("****"));
    }

    @Test
    void whenNotExistRetrieveTransaction_thenReturns404() throws Exception {
        Long invoiceId = 1L;

        when(paymentService.findPayment(invoiceId)).thenReturn(null);
        mockMvc.perform(get("/payment")
                        .contentType("application/json")
                        .param("invoiceId", String.valueOf(invoiceId)))
                .andExpect(status().isNotFound());
    }
}
